#!/usr/bin/perl -w
sub read_tree;
sub handle_dir;
sub relative_to;
sub print_help;

# basedir is top of web source tree
# contentdir is where input for web site comes from
# templatesdir has templates for various types of pages on website
# htmldir is where output, processed html goes
$basedir = "/usr/local/www/i18n.gnome.org";
$contentdir = "$basedir/content";
$templatesdir = "$basedir/templates";
$htmldir = "/usr/local/www/gnomeweb/gnomecvs/i18n-web/obj/html";
$contentfile = "main.in";
$outputfile = "index.html";
$leaf="";
$titletag = "Title";

# Variables that must be substituted.
@possible_vars = ("last_modified");

#
# figure out where we are relative to the entire web src tree
#
chomp($curdir = `pwd`);
$curdir =~ s|^$contentdir||;
@dirpieces = split("/", $curdir);
$title = "";
#Parse the arguments!!!
foreach $i (@ARGV) {
    if ($i eq "--help") {
	print_help ();
	exit 0;
    } elsif ($i =~ "--content=.*") {
	$i =~ s|^--content=||;
	$contentfile = $i;
#	print ("--content=$contentfile\n");
    } elsif ($i =~ "--leaf") {
	$i =~ s|^--leaf=||;
	$leaf = "$curdir/$i";
	$outputfile = $i;
#	print ("--leaf=$leaf\n");
    } else {
	print_help ();
	exit 1;
    }
}
if ($leaf eq "") {
    $leaf = $curdir;
}

open (BOILER, "$templatesdir/boiler.tmpl")|| die ("Couldn't open $templatesdir/boiler.tmpl");
$boilerplate .= $_ while(<BOILER>);
close (BOILER);

$content = "";
# This coe should be rewritten in future - it's not flexiable
if (open (MAIN, $contentfile)) {
    while (<MAIN>) {
      # And we guess, that this is one line command....
      if(m|<!--\#set\s+var="([^"]+)"\s+value="([^"]+)"\s+-->|gi) {
	$varval = $2;
	$varname = lc($1);
	if($varname eq "last_modified") { $varval =~ s/^\$DATE:\s+(.*)\s+\$$/$1/i; }
	$varvals{$varname} = $varval;
      }
      $content .= $_;
    }
    close (MAIN);
}
    
#
# create output file
#
$newdir = "$htmldir";
# Cut trailing slash(es)
$newdir =~ s|(.*)/?/?|$1|;
# If this directory doesn't exists - create it
unless(-e $newdir) {
    mkdir($newdir, 0755) || die("Can't create output directory $newdir");
}
# Add trailing slash
$newdir .= '/';

# Recursevly create nested directories
foreach my $dir (@dirpieces) {
    # Skip empty elements of the array
    next unless($dir);
    $newdir .= $dir;
    unless(-e $newdir) {
	mkdir($newdir, 0755) || die("Can't create output directory $newdir");
    }
    $newdir .= '/';
}

open (OUTFILE, ">$newdir/$outputfile") || die ("Couldn't open output file $newdir/$outputfile");

#
# read in template for a branch page in the web tree, subst values into it
#

$menu = handle_dir(0);
$boilerplate =~ s|\@TITLE\@|$title|g;
$boilerplate =~ s|\@MENU\@|$menu|g;
$boilerplate =~ s|\@CONTENT\@|$content|g;
foreach $avar (@possible_vars) {
    # If we do have any value
    if($aval = $varvals{$avar}){
	$boilerplate =~ s|\@$avar\@|$aval|g;
    }
}
print OUTFILE $boilerplate;

# all done
close (OUTFILE);


#
# END
#

#
# Subroutines follow, no more main code below here
#
sub handle_dir {
  my($i) = @_;
  my($retval) = "";
  my($href) = "";

  if($i > scalar(@dirpieces)) { return; }
  my %pieces;
  if (@dirpieces == 0) {
      %pieces = read_tree($contentdir);
  } else {
      %pieces = read_tree("${contentdir}/" . join("/", @dirpieces[0..$i]));
  }
  my(@sorteddirs) = @{$pieces{"__dirs__"}};

  my($j);
  for($j = 0; $j < scalar(@sorteddirs); $j++) {
#      $retval .= $itemstart;
      for ($k=0; $k < $i; $k++) {
          $retval .= "&nbsp; &nbsp; &nbsp; ";
      }

      $href = relative_to($sorteddirs[$j], $i);

      # Evil hack to make the base work.
      if ($href =~ /.*BASE.*/) {
	  if ($leaf eq "") {
	      $retval .= sprintf("%s", $pieces{$sorteddirs[$j]});
	  } else {
	      $retval .= sprintf("<a href=\"/\">%s</a>", $pieces{$sorteddirs[$j]});
	  }
      } else {
	  $temp = $href;
	  $temp =~ s|/$||; #strip off the last '/' to make our comparison.  It is basicly /arch/gnome-webs
	  $temp2 = "$temp/index.html";
	  $temp3 = "/$href";

	  if ($temp eq $leaf || $temp2 eq $leaf || $temp3 eq $leaf){
	      $retval .= $pieces{$sorteddirs[$j]};
	  } else {
	      $retval .= sprintf("<a href=\"%s\">%s</a>", $href,$pieces{$sorteddirs[$j]});
	  }
      }
      $retval .= "<br>\n";
      if((scalar (@dirpieces) != 0) &&
	 (scalar (@dirpieces) > ($i + 1)) &&
	 ($sorteddirs[$j] eq $dirpieces[$i + 1])) {
	  $retval .= handle_dir($i + 1);
      }
  }
  return $retval;
}


sub read_tree {
  my($fn) = @_;
  my(%retval, @dirs);

  open(FH, "$fn/tree.in") || die("Couldn't open $fn/tree.in");

  foreach(<FH>) {
    chomp($_);
    if($_ !~ /;/) { next; }
    my($label, $dir) = split(/;/, $_, 2);
    if ($label eq $titletag) {
	$title = $dir;
    } else {
	$retval{$dir} = $label;
	push(@dirs, $dir);
    }
  }

  close(FH);
  @{$retval{"__dirs__"}} = @dirs;
  return %retval;
}

sub relative_to {
  my($subdir, $curlevel) = @_;

  if($subdir =~ m|://|) {
	return $subdir;
  } elsif (@dirpieces != 0) {
      if ($subdir =~ /.*html/) {
	  return join("/", @dirpieces[0..$curlevel], $subdir);
      } else {
	  return join("/", @dirpieces[0..$curlevel], $subdir) . "/";
      }
  } else {
      if ($subdir =~ /.*html/) {
	  return "$subdir";
      } else {
	  return "$subdir/";
      }
  }
}
sub print_help {
 print ("Usage: makenode.pl [OPTIONS]\n\n\t--help\t\t\tprints out this message\n\t--contents=[FILE]\tsets the contents of the page to be FILE\n\t--leaf=[STRING]\t\tUndocumented secret\n\n");
}

